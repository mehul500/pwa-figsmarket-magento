/**
 * Copyright © 2020 Aureatelabs. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'myfatoorah',
                component: 'Aureatelabs_MyFatoorah/js/view/payment/method-renderer/myfatoorah'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);
