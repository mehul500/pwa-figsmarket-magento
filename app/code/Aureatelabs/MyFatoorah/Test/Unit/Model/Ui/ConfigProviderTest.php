<?php
/**
 * Copyright © 2020 Aureatelabs. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aureatelabs\MyFatoorah\Test\Unit\Model\Ui;

use Aureatelabs\MyFatoorah\Gateway\Http\Client\ClientMock;
use Aureatelabs\MyFatoorah\Model\Ui\ConfigProvider;

class ConfigProviderTest extends \PHPUnit_Framework_TestCase
{
    public function testGetConfig()
    {
        $configProvider = new ConfigProvider();

        static::assertEquals(
            [
                'payment' => [
                    ConfigProvider::CODE => [
                        'transactionResults' => [
                            ClientMock::SUCCESS => __('Success'),
                            ClientMock::FAILURE => __('Fraud')
                        ]
                    ]
                ]
            ],
            $configProvider->getConfig()
        );
    }
}
