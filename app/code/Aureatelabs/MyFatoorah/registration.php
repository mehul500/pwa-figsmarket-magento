<?php
/**
 * Copyright © 2020 Aureatelabs. All rights reserved.
 * See COPYING.txt for license details.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Aureatelabs_MyFatoorah',
    __DIR__
);
